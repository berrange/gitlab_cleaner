#!/usr/bin/env python3

import gitlab  # needs pip install python-gitlab first!

import argparse
import datetime as dt
import logging
import os
import sys

log = None


class Pipelines:
    def __init__(self, token, gitlab_id=None, project_id=None):
        self._projects = []
        self._gl = gitlab.Gitlab(private_token=token)
        if project_id:
            self._projects = [self._gl.projects.get(id=project_id)]
        else:
            # try looking up the namespace in groups first
            try:
                ns = self._gl.groups.get(id=gitlab_id)
            except gitlab.GitlabGetError:
                try:
                    ns = self._gl.users.get(id=gitlab_id)
                except gitlab.GitlabGetError as e:
                    log.error(f"Failed to query namespace ID '{gitlab_id}': {e}")
                    sys.exit(1)

            try:
                ns_projects = ns.projects.list(all=True)
            except gitlab.GitlabHttpError as e:
                log.error(f"Failed to query the list of projects: {e}")
                sys.exit(1)

            for ns_project in ns_projects:
                # 'ns_project' may be of type
                # gitlab.v4.objects.projects.GroupProject OR
                # gitlab.v4.objects.projects.UserProject, but these types don't
                # have all the data we need, so we need to query a specific
                # object of type gitlab.v4.objects.projects.Project instead
                self._projects.append(self._gl.projects.get(ns_project.id))

    def clean(self,
              before_date,
              dryrun=False):
        for proj in self._projects:
            if proj.archived:
                log.info(f"{proj.name} ({proj.id}): SKIP [archived]")
                continue

            done = False
            while not done:
                # Instead of iterating over a list which can be huge, we're
                # going to get a generator object pointing to the pipelines.
                # However, since we're going to be removing pipelines GitLab is
                # going to be re-adjusting the result pages with which the
                # generator simply can't cope and the result being that some
                # pipeline entries simply won't be deleted after the first pass
                # (because GitLab moved them to an earlier page which the
                # generator had already queried and passed onto us).
                #
                # To truly remove *all* the desired entries, we'll need to do
                # more passes
                pipelines = proj.pipelines.list(all=True,
                                                as_list=False,
                                                updated_before=before_date)
                done = True
                for pipeline in pipelines:
                    log.info((f"{proj.name} ({proj.id}): [pipelines]: DELETE "
                              f"{pipeline.id} created at "
                              f"{pipeline.attributes['created_at']}"))
                    try:
                        if not dryrun:
                            # 'pipelines' is a generator object and there's no
                            # way to determine if a generator object is empty
                            # other than exhausting it first so we set 'done'
                            # to False in each iteration
                            done = False
                            pipeline.delete()
                    except gitlab.GitlabDeleteError:
                        # reconnect may help
                        pass
            log.info(f"{proj.name} ({proj.id}): DONE")

    def list(self, before_date=dt.datetime.utcnow().isoformat()):
        for proj in self._projects:
            if proj.archived:
                log.info(f"{proj.name} ({proj.id}): SKIP [archived]")
                continue

            for p in proj.pipelines.list(all=True,
                                         as_list=True,
                                         updated_before=before_date):
                print(f"{proj.name} ({proj.id}): pipeline {p.id}",
                      f"created at {p.attributes['created_at']}")


def run(args):
    token = None
    gitlab_id = None
    project_id = None
    before = dt.datetime.utcnow().isoformat()

    if args.token_file:
        try:
            # the file has already been opened by argparse
            token = args.token_file.read().strip()
        except FileNotFoundError as e:
            log.error(e)
            sys.exit(1)

    if args.gitlab_id:
        gitlab_id = args.gitlab_id

    if args.project_id:
        project_id = args.project_id

    # CLI options have precedence over env variables
    token = os.environ.get("GITLAB_TOKEN", token)
    gitlab_id = os.environ.get("GITLAB_ID", gitlab_id)
    project_id = os.environ.get("GITLAB_PROJECT_ID", project_id)

    if not token:
        log.error("Missing GitLab API token")
        sys.exit(1)

    if gitlab_id is None and project_id is None:
        log.error("Either a namespace ID or a project ID must be specified")
        sys.exit(1)

    if args.days:
        tdelta = dt.timedelta(days=args.days)
        before = (dt.datetime.utcnow() - tdelta).isoformat()

    if args.before:
        try:
            before = dt.datetime.fromisoformat(args.before)
        except ValueError as e:
            log.error(e)
            sys.exit(1)

    gl_pipelines = Pipelines(token, gitlab_id, project_id)
    if args.clean:
        gl_pipelines.clean(before, args.dryrun)
    elif args.list:
        gl_pipelines.list(before)


def setup_logging(args):
    global log

    logging.basicConfig(level=logging._nameToLevel[args.log_level])
    formatter = logging.Formatter("[%(levelname)s]: %(message)s")
    log = logging.getLogger()
    if args.quiet:
        # Note that the 'disabled' attr is not public API, but does the job
        log.disabled = True
    handler = log.handlers[0]
    handler.setFormatter(formatter)


def argparse_check_positive(value):
    val = int(value)
    if val <= 0:
        raise argparse.ArgumentTypeError(f"{val} is not a positive integer")
    return val


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--log-level",
        type=str,
        choices=sorted(logging._nameToLevel.keys()),
        default="INFO",
        help="log level (default is INFO)",
    )

    parser.add_argument(
        "-q", "--quiet",
        action="store_true",
        default=False,
        help="silence all output",
    )

    parser.add_argument(
        "--dry-run",
        dest="dryrun",
        action="store_true",
        default=False,
        help="only print the action, don't perform it",
    )

    parser.add_argument(
        "-f", "--token-from-file",
        dest="token_file",
        type=argparse.FileType("r"),
        help="file from which the API token should be read",
    )

    ns_group = parser.add_mutually_exclusive_group(required=True)
    ns_group.add_argument(
        "-I", "--namespace-id",
        dest="gitlab_id",
        type=str,
        help=("GitLab namespace ID (group/personal namespace),"
              "this will target every project under the namespace"),
    )

    ns_group.add_argument(
        "-P", "--project-id",
        type=str,
        help="GitLab project ID",
    )

    subparsers = parser.add_subparsers(
        metavar="OBJECT",
        dest="obj",
    )
    subparsers.required = True

    pipelines_parser = subparsers.add_parser(
        "pipelines",
        help="operate over a set of pipelines",
    )

    lc_group = pipelines_parser.add_mutually_exclusive_group(required=True)
    lc_group.add_argument(
        "--list",
        action="store_true",
        help="list the filtered pipelines"
    )

    lc_group.add_argument(
        "--clean",
        action="store_true",
        help="clean the filtered pipelines"
    )

    ts_group = pipelines_parser.add_mutually_exclusive_group(required=False)
    ts_group.add_argument(
        "--all",
        action="store_true",
        help="filter all pipelines (this is the default)"
    )

    ts_group.add_argument(
        "-d", "--days",
        type=argparse_check_positive,
        help="filter pipelines older than N days",
    )

    ts_group.add_argument(
        "-B", "--before",
        metavar="TIMESPEC",
        type=str,
        help="filter pipelines older than <ISO 8601 timespec>",
    )

    args = parser.parse_args()
    setup_logging(args)
    run(args)
