==============
gitlab cleaner
==============

This repository provides a simple tool called ``gitlab-cleaner`` which aims
to clean various GitLab artifacts (currently only pipelines) that cannot be
done using GitLab's web UI in bulk.

Prerequisites
=============
For software prerequisites, please follow with:

::

    $ pip3 install --user -r requirements.txt

OR

::

    $ python3 -m venv <path_to_venv>
    $ source <path_to_venv>/bin/activate
    $ pip install -t requirements.txt

Other than that you'll need to create a personal access token with full API
access in GitLab's web UI. The token is only displayed once, so save it
somewhere safe.

First steps
===========

Since exposing the API token (with full access rights) in Bash history would be
a security risk, you can supply your token either via the ``GITLAB_TOKEN``
environment variable or with the ``--token-from-file`` CLI option.
Next you'll need either the namespace ID (a group or your personal namespace ID
depending on what scale you want to perform the cleanup) or specifically the
project ID that you want to work with.


Examples
========

To list all pipelines that would be cleaned up in project ``ID:123456``, run

::

    $ ./gitlab_cleaner.py -P 123456 -f tokenfile pipelines --list

To clean pipelines in project ``ID:123456`` older than 30 days, run

::

    $ ./gitlab_cleaner.py -P 123456 -f tokenfile pipelines --clean --days 30

To clean pipelines older than e.g. ``July 15th 2022`` in **all**
projects under the namespace ``ID:456789``, first do a dry-run:

::

    $ ./gitlab_cleaner.py \
        -I 456789 \
        -f tokenfile \
        --dry-run \
        pipelines \
            --clean \
            --before "$(date -d '15 Jul 2022' --iso-8601)"

and confirm that the listing matches your expectations, then proceed with:

::

    $ ./gitlab_cleaner.py \
        -I 456789 \
        -f tokenfile \
        pipelines \
            --clean \
            --before "$(date -d '15 Jul 2022' --iso-8601)"

which will iterate over all projects and their respective pipelines and remove
the filtered subset.
In order to silence the logging, either set a lower log level:

::

    $ ./gitlab_cleaner.py \
        --log-level ERROR \
        -I 456789 \
        -f tokenfile \
        pipelines \
            --clean \
            --before "$(date -d '15 Jul 2022' --iso-8601)"

or simply use ``--quiet`` (won't print any log output):

::

    $ ./gitlab_cleaner.py \
        --log-level ERROR \
        -I 456789 \
        -f tokenfile \
        pipelines \
            --clean \
            --before "$(date -d '15 Jul 2022' --iso-8601)"
